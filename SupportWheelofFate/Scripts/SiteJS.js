﻿$(document).ready(function () {
    $("#engineersContainer").hide();
    $("#messageContainer").hide();

    $("#SeeEngineers").click(function (e) {
        var me = $(this);
        e.preventDefault();

        if (me.data('requestRunning')) {
            return;
        }

        me.data('requestRunning', true);

        preClickActions();
        $.ajax({
            type: "POST",
            url: "/Engineer/GetSelectedEngineers/",
            success: function (result) {
                if (result.engineers) {
                    selectedEngineers = result;
                    setTimeout(showSelectedEngineers, 1000);
                }
                else {
                    showError('There\'s no engineers selected today yet');
                }
            },
            complete: function () {
                me.data('requestRunning', false);
            }
        });
    });

    $("#GetEngineers").click(function (e) {
        var me = $(this);
        e.preventDefault();

        if (me.data('requestRunning')) {
            return;
        }

        me.data('requestRunning', true);

        preClickActions();
        $.ajax({
            type: "POST",
            url: "/Engineer/GetEngineers/",
            success: function (result) {
                if (result.engineers) {
                    selectedEngineers = result;
                    setTimeout(showSelectedEngineers, 1000);
                }
                else
                {
                    showError('Two engineers has already been selected for today!');
                }
            },
            complete: function () {
                me.data('requestRunning', false);
            }
        });
    });
});

function showError(message) {
    $("#loaderContainer").hide();
    $("#messageContainer span").text(message);
    $("#messageContainer").show();
}

function showSelectedEngineers() {
    $("#loaderContainer").hide();

    if (selectedEngineers) {
        for (eng in selectedEngineers.engineers) {
            $("#engineersContainer").append("<div class=\"row text-center\"><span class=\"text-uppercase\">" + selectedEngineers.engineers[eng].Name + "," + selectedEngineers.engineers[eng].Lastname + "</span></div>");
        }
    }    
    $("#engineersContainer").show();
}

function preClickActions() {
    $("#messageContainer").hide();
    $("#engineersContainer").hide();
    $("#engineersContainer").html("");
    $("#loaderContainer").show();
    selectedEngineers = null;
}
