﻿using SupportWheelOfFate.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace SupportWheelofFate.Models
{
    public class EngineerClient
    {
        private string Base_URL = "http://localhost:50631/api/bau/";

        public IEnumerable<Engineer> getLuckyEngineers()
        {
            try
            {
                var APIServer = new Uri(Base_URL);
                HttpClient client = new HttpClient();
                client.BaseAddress = APIServer;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                
                HttpResponseMessage response = client.GetAsync("getluckyengineers").Result;

                if (response.IsSuccessStatusCode)
                    return response.Content.ReadAsAsync<IEnumerable<Engineer>>().Result;
                return null;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<Engineer> getSelectedEngineers()
        {
            try
            {
                var APIServer = new Uri(Base_URL);
                HttpClient client = new HttpClient();
                client.BaseAddress = APIServer;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = client.GetAsync("getSelectedEngineers").Result;

                if (response.IsSuccessStatusCode)
                    return response.Content.ReadAsAsync<IEnumerable<Engineer>>().Result;
                return null;
            }
            catch
            {
                return null;
            }
        }
    }
}