﻿using System.Collections.Generic;
using SupportWheelOfFate.Model;

namespace SupportWheelofFate.ViewModel
{
    public class EngineerViewModel
    {
        public List<Engineer> engineers { get; set; }
    }
}