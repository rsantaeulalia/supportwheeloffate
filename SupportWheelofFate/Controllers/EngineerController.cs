﻿using System.Linq;
using System.Web.Mvc;
using SupportWheelofFate.Models;
using SupportWheelofFate.ViewModel;

namespace SupportWheelofFate.Controllers
{
    public class EngineerController : Controller
    {
        // GET: Engineer  
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetEngineers()
        {
            EngineerClient EC = new EngineerClient();
            EngineerViewModel evm = new EngineerViewModel();
            var engineers = EC.getLuckyEngineers();

            evm.engineers = engineers != null ? engineers.ToList() : null;

            return Json(evm);
        }

        [HttpPost]
        public ActionResult GetSelectedEngineers()
        {
            EngineerClient EC = new EngineerClient();
            EngineerViewModel evm = new EngineerViewModel();
            var engineers = EC.getSelectedEngineers();

            evm.engineers = engineers != null ? engineers.ToList() : null;

            return Json(evm);
        }

        //public ActionResult Delete(int id)
        //{
        //    CustomerClient CC = new CustomerClient();
        //    CC.Delete(id);
        //    return RedirectToAction("Index");
        //}
        //[HttpGet]
        //public ActionResult Edit(int id)
        //{
        //    CustomerClient CC = new CustomerClient();
        //    CustomerViewModel CVM = new CustomerViewModel();
        //    CVM.customer = CC.find(id);
        //    return View("Edit", CVM);
        //}
        //[HttpPost]
        //public ActionResult Edit(CustomerViewModel CVM)
        //{
        //    CustomerClient CC = new CustomerClient();
        //    CC.Edit(CVM.customer);
        //    return RedirectToAction("Index");
        //}
    }
}
