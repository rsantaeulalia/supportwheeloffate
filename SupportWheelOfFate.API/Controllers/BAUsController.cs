﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Http;
using SupportWheelOfFate.Entities;

namespace SupportWheelOfFate.API.Controllers
{
    [RoutePrefix("api/bau")]
    public class BAUsController : ApiController
    {
         private WheelOfFateEntities db = new WheelOfFateEntities();

        [Route("getLuckyEngineers")]
        public List<Model.Engineer> GetLuckyEngineers()
        {
            try
            {
                //check that dont exists a BAU for this day
                if (!db.BAUs.ToList().Any(x => x.date.ToUniversalTime() == DateTime.Today.ToUniversalTime()))
                {
                    //Select the last period to check engineers that doesn't complete a entire support date in that date range
                    var period = db.PeriodConfigs.OrderByDescending(x => x.id).FirstOrDefault();

                    //Check if today is a new period or not, in that case, we create a new period
                    if (DateTime.Today > period.endPeriod)
                    {
                        var newPeriod = new PeriodConfig();
                        newPeriod.startPeriod = DateTime.Today;
                        newPeriod.endPeriod = DateTime.Today.AddDays(15);
                        period = db.PeriodConfigs.Add(newPeriod);
                    }

                    var baus = db.BAUs.Where(x => x.PeriodConfig.id == period.id);

                    //filter first for the engineers who don't has at least one complete day on 2 wheeks period
                    List<Engineer> engineers = db.Engineers.ToList();
                    List<Model.Engineer> pickableEngineers = new List<Model.Engineer>();
                    foreach (var eng in engineers)
                    {
                        if (baus.Count(x => x.idEngineer1 == eng.id) + baus.Count(x => x.idEngineer2 == eng.id) < 2)
                        {
                            var m = new Model.Engineer();
                            m.Name = eng.name;
                            m.Lastname = eng.lastname;
                            m.Id = eng.id;
                            pickableEngineers.Add(m);
                        }
                    }

                    //then filter for the engineers who dont has two support hours shift, so I took the previous BAU
                    var previousBau = baus.OrderByDescending(x => x.id).FirstOrDefault();

                    var removeEngineer = pickableEngineers.FirstOrDefault(x => x.Id == previousBau.FirstEngineer.id);
                    pickableEngineers.Remove(removeEngineer);

                    removeEngineer = pickableEngineers.FirstOrDefault(x => x.Id == previousBau.SecondEngineer.id);
                    pickableEngineers.Remove(removeEngineer);

                    //then select randomly two engineers from that list
                    var selectedEngineers = APIHelper.PickRandom(pickableEngineers, 2);

                    //then create a BAU for this day
                    BAU b = new BAU();
                    b.idEngineer1 = selectedEngineers[0].Id;
                    b.idEngineer2 = selectedEngineers[1].Id;
                    b.PeriodConfig = period;
                    b.date = DateTime.Today;
                    db.BAUs.Add(b);

                    db.SaveChanges();

                    return selectedEngineers;
                }

                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Route("getSelectedEngineers")]
        public List<Model.Engineer> GetSelectedEngineers()
        {
            try
            {
                //check that exists a BAU for this day
                if (db.BAUs.ToList().Any(x => x.date.ToUniversalTime() == DateTime.Today.ToUniversalTime()))
                {
                    var bau = db.BAUs.ToList().FirstOrDefault(x => x.date.ToUniversalTime() == DateTime.Today.ToUniversalTime());

                    List<Model.Engineer> selectedEngineers = new List<Model.Engineer>();

                    var m = new Model.Engineer();
                    m.Name = bau.FirstEngineer.name;
                    m.Lastname = bau.FirstEngineer.lastname;
                    m.Id = bau.FirstEngineer.id;
                    selectedEngineers.Add(m);

                    m = new Model.Engineer();
                    m.Name = bau.SecondEngineer.name;
                    m.Lastname = bau.SecondEngineer.lastname;
                    m.Id = bau.SecondEngineer.id;
                    selectedEngineers.Add(m);

                    return selectedEngineers;
                }

                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}