﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SupportWheelOfFate.Entities
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class WheelOfFateEntities : DbContext
    {
        public WheelOfFateEntities()
            : base("name=WheelOfFateEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<BAU> BAUs { get; set; }
        public virtual DbSet<Engineer> Engineers { get; set; }
        public virtual DbSet<PeriodConfig> PeriodConfigs { get; set; }
    }
}
