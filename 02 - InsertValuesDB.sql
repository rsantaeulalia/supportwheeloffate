USE [WheelOfFate]
GO
SET IDENTITY_INSERT [dbo].[Engineers] ON 

INSERT [dbo].[Engineers] ([id], [name], [lastname]) VALUES (1, N'John', N'Mayer')
INSERT [dbo].[Engineers] ([id], [name], [lastname]) VALUES (2, N'David', N'Gilmour')
INSERT [dbo].[Engineers] ([id], [name], [lastname]) VALUES (3, N'Roger', N'Waters')
INSERT [dbo].[Engineers] ([id], [name], [lastname]) VALUES (4, N'Steve', N'Vai')
INSERT [dbo].[Engineers] ([id], [name], [lastname]) VALUES (1002, N'Joe', N'Satriani')
INSERT [dbo].[Engineers] ([id], [name], [lastname]) VALUES (1003, N'Jimi', N'Hendrix')
INSERT [dbo].[Engineers] ([id], [name], [lastname]) VALUES (1004, N'Eric', N'Clapton')
INSERT [dbo].[Engineers] ([id], [name], [lastname]) VALUES (1005, N'Angus ', N'Young')
INSERT [dbo].[Engineers] ([id], [name], [lastname]) VALUES (1006, N'Jeff', N'Beck')
INSERT [dbo].[Engineers] ([id], [name], [lastname]) VALUES (1007, N'Brian', N'May')
SET IDENTITY_INSERT [dbo].[Engineers] OFF
SET IDENTITY_INSERT [dbo].[PeriodConfig] ON 

INSERT [dbo].[PeriodConfig] ([id], [startPeriod], [endPeriod]) VALUES (1, CAST(N'2018-06-01' AS Date), CAST(N'2018-06-16' AS Date))
SET IDENTITY_INSERT [dbo].[PeriodConfig] OFF
SET IDENTITY_INSERT [dbo].[BAU] ON 

INSERT [dbo].[BAU] ([id], [idEngineer1], [idEngineer2], [date], [idPeriod]) VALUES (1, 1, 2, CAST(N'2018-06-01T00:00:00.000' AS DateTime), 1)
INSERT [dbo].[BAU] ([id], [idEngineer1], [idEngineer2], [date], [idPeriod]) VALUES (2, 3, 4, CAST(N'2018-06-02T00:00:00.000' AS DateTime), 1)
INSERT [dbo].[BAU] ([id], [idEngineer1], [idEngineer2], [date], [idPeriod]) VALUES (3, 2, 1, CAST(N'2018-06-03T00:00:00.000' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[BAU] OFF
