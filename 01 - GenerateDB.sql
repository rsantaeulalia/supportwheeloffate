CREATE DATABASE [WheelOfFate];
GO

USE [WheelOfFate]
GO
/****** Object:  Table [dbo].[BAU]    Script Date: 4/6/2018 16:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BAU](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idEngineer1] [int] NOT NULL,
	[idEngineer2] [int] NOT NULL,
	[date] [datetime] NOT NULL,
	[idPeriod] [int] NOT NULL,
 CONSTRAINT [PK_BAU] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Engineers]    Script Date: 4/6/2018 16:39:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Engineers](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[lastname] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Engineers] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PeriodConfig]    Script Date: 4/6/2018 16:39:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PeriodConfig](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[startPeriod] [date] NOT NULL,
	[endPeriod] [date] NOT NULL,
 CONSTRAINT [PK_PeriodConfig] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BAU]  WITH CHECK ADD  CONSTRAINT [FK_BAU_Engineers] FOREIGN KEY([idEngineer1])
REFERENCES [dbo].[Engineers] ([id])
GO
ALTER TABLE [dbo].[BAU] CHECK CONSTRAINT [FK_BAU_Engineers]
GO
ALTER TABLE [dbo].[BAU]  WITH CHECK ADD  CONSTRAINT [FK_BAU_Engineers1] FOREIGN KEY([idEngineer2])
REFERENCES [dbo].[Engineers] ([id])
GO
ALTER TABLE [dbo].[BAU] CHECK CONSTRAINT [FK_BAU_Engineers1]
GO
ALTER TABLE [dbo].[BAU]  WITH CHECK ADD  CONSTRAINT [FK_BAU_PeriodConfig] FOREIGN KEY([idPeriod])
REFERENCES [dbo].[PeriodConfig] ([id])
GO
ALTER TABLE [dbo].[BAU] CHECK CONSTRAINT [FK_BAU_PeriodConfig]
GO
